import cv2
import numpy as np
import os
import pydicom
import torch

from PIL import Image
from torchvision import transforms

imagenet_normalize = transforms.Normalize(
    mean=torch.tensor([0.485, 0.456, 0.406]),
    std=torch.tensor([0.229, 0.224, 0.225])
)


def uint16_to_uint8(img_arr, norm_hist=True):
    """
    Converts an uint16 image into a uint8 image.

    Args:
    img_arr (np.ndarray): uint16 image array to be converted.
    norm_hist (bool): If should perform histogram equalization in the input image before conversion.
    """
    max_val = np.amax(img_arr)
    img_arr = img_arr.astype(float) * 255 / max_val
    img_arr = img_arr.astype(np.uint8)

    if norm_hist:
        img_arr = cv2.equalizeHist(img_arr)

    return img_arr.astype(np.uint8)


def load_dcm(filepath, norm_hist=False):
    ds = pydicom.dcmread(filepath)
    img = uint16_to_uint8(ds.pixel_array, norm_hist=norm_hist)
    return img


class InputLoaderMixin:
    @staticmethod
    def read_dcm(dcm_fpath, mode="RGB"):
        ds = pydicom.dcmread(dcm_fpath, force=True)
        img_array = ds.pixel_array

        if len(img_array.shape) > 2:
            raise ValueError("Unsupported multi-frame DICOM.")

        img = Image.fromarray(uint16_to_uint8(ds.pixel_array))

        return img.convert(mode)

    @staticmethod
    def read_npy(npy_fpath, mode="RGB"):
        img_array = np.load(npy_fpath)
        img = Image.fromarray(uint16_to_uint8(img_array))

        return img.convert(mode)

    @staticmethod
    def _load_input_rgb(R_filepath, G_filepath, B_filepath):
        to_tensor = transforms.ToTensor()
        to_pil = transforms.ToPILImage()

        _, ext = os.path.basename(G_filepath).rsplit(".", maxsplit=1)
        load_fn = getattr(InputLoaderMixin, f"read_{ext}", InputLoaderMixin.read_dcm)

        g = load_fn(G_filepath, "L")
        g_arr = to_tensor(g)

        r = load_fn(R_filepath, "L") if R_filepath is not None else None
        r_arr = to_tensor(r) if r is not None else g_arr

        b = load_fn(B_filepath, "L") if B_filepath is not None else None
        b_arr = to_tensor(b) if b is not None else g_arr

        rgb = torch.cat([r_arr, g_arr, b_arr], dim=0)
        return to_pil(rgb)

    @staticmethod
    def _load_input_gray(R_filepath, G_filepath, B_filepath):
        _, ext = os.path.basename(G_filepath).rsplit(".", maxsplit=1)
        load_fn = getattr(InputLoaderMixin, f"read_{ext}", InputLoaderMixin.read_dcm)

        return load_fn(G_filepath)

    @classmethod
    def load_input(cls, R_filepath, G_filepath, B_filepath, mode, resize=None):
        load_fn = getattr(cls, f"_load_input_{mode}")
        input_ = load_fn(R_filepath, G_filepath, B_filepath)

        if resize:
            input_ = resize(input_)

        return input_
